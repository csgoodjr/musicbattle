import React from "react";
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import "./App.css";
import Home from "./views/Home";

function App() {
  return (
    <div className="App">
      <Router>
        <div>
          <nav>
            <Link to="/">Home</Link>
            <Link to="/name-that-tune">Name That Tune!</Link>
            <Link to="/music-battle">Music Battle!</Link>
            <Link to="/leaderboard">Leaderboard</Link>
          </nav>
          <Switch>
            <Route path="/name-that-tune"></Route>
            <Route path="/music-battle"></Route>
            <Route path="/leaderboard"></Route>
            <Route path="/">
              <Home />
            </Route>
          </Switch>
        </div>
      </Router>
    </div>
  );
}

export default App;
