import React from "react";
import clsx from "clsx";

interface HomeProps {
  className?: string;
}

const Home: React.FC<HomeProps> = ({ className = "" }) => {
  return (
    <div className={clsx(className)}>
      <h4>Home</h4>
    </div>
  );
};

export default Home;
